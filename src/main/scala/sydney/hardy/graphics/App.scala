package sydney.hardy.graphics

import org.slf4j.LoggerFactory
import sydney.hardy.graphics.opencl.Platform

object App {
  private val LOG = LoggerFactory.getLogger(classOf[App])

  def main(args: Array[String]): Unit = {
    for (platform <- Platform.all) {
      LOG.info("Name: \"{}\"", platform.name)
      LOG.info("  Vendor: \"{}\"", platform.vendor)
      LOG.info("  Version: \"{}\"", platform.version)
      LOG.info("  Extensions: \"{}\"", platform.extensions)
      LOG.info("  Profile: \"{}\"", platform.profile)
      LOG.info("  Devices:")
      for (device <- platform.devices) {
        LOG.info("    {}:", device.id)
        LOG.info("      Name: \"{}\"", device.name)
        LOG.info("      Type: \"{}\"", device.deviceType)
        LOG.info("      Vendor: \"{}\"", device.vendor)
        LOG.info("      VendorId: \"{}\"", device.vendorId)
        LOG.info("      DriverVersion: \"{}\"", device.driverVersion)
        LOG.info("      Extensions: \"{}\"", device.extensions)
        LOG.info("      Profile: \"{}\"", device.profile)
        LOG.info("      MaxComputeUnits: \"{}\"", device.maxComputeUnits)
        LOG.info("      MaxClockFrequency: \"{}\"", device.maxClockFrequency)
        LOG.info("      LocalMemorySize: \"{}\"", device.localMemorySize)
        LOG.info("      GlobalMemorySize: \"{}\"", device.globalMemorySize)
        LOG.info("      GlobalMemoryCacheSize: \"{}\"", device.globalMemoryCacheSize)
        LOG.info("      AddressBits: \"{}\"", device.addressBits)
        LOG.info("      MaxImageDimensions2D: \"{}x{}\"", device.image2DMaxWidth, device.image2DMaxHeight)
        LOG.info("      MaxImageDimensions3D: \"{}x{}x{}\"", device.image3DMaxWidth, device.image3DMaxHeight, device.image3DMaxDepth)
        LOG.info("      Max3DImageSamplers: \"{}\"", device.image3DMaxSamplers)
        LOG.info("      MaxWorkItemDimensions: \"{}\"", device.maxWorkItemDimensions)
        LOG.info("      MaxWorkItemSizes: \"{}\"", device.maxWorkItemSizes)
        LOG.info("      WorkGroupSize: \"{}\"", device.workGroupSize)
      }
    }
  }
}
