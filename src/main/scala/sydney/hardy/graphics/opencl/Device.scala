package sydney.hardy.graphics.opencl

import java.nio.charset.Charset

import org.jocl.{CL, Pointer, cl_device_id}

object Device {
  val MAX_DEVICES = 100
}

class Device(val id: cl_device_id, val platform: Platform) {
  lazy val name: String = getDeviceStringParam(CL.CL_DEVICE_NAME)

  lazy val deviceType = new DeviceType(getDeviceLongParam(CL.CL_DEVICE_TYPE))

  lazy val vendor: String = getDeviceStringParam(CL.CL_DEVICE_VENDOR)

  lazy val vendorId: Int = getDeviceIntParam(CL.CL_DEVICE_VENDOR_ID)

  lazy val version: String = getDeviceStringParam(CL.CL_DEVICE_VERSION)

  lazy val driverVersion: String = getDeviceStringParam(CL.CL_DRIVER_VERSION)

  lazy val extensions: String = getDeviceStringParam(CL.CL_DEVICE_EXTENSIONS)

  lazy val profile: String = getDeviceStringParam(CL.CL_DEVICE_PROFILE)

  lazy val maxComputeUnits: Int = getDeviceIntParam(CL.CL_DEVICE_MAX_COMPUTE_UNITS)

  /**
   * in MHz
   */
  lazy val maxClockFrequency: Int = getDeviceIntParam(CL.CL_DEVICE_MAX_CLOCK_FREQUENCY)

  lazy val localMemorySize: Long = getDeviceLongParam(CL.CL_DEVICE_LOCAL_MEM_SIZE)

  lazy val globalMemorySize: Long = getDeviceLongParam(CL.CL_DEVICE_GLOBAL_MEM_SIZE)

  lazy val globalMemoryCacheSize: Long = getDeviceLongParam(CL.CL_DEVICE_GLOBAL_MEM_CACHE_SIZE)

  lazy val addressBits: Int = getDeviceIntParam(CL.CL_DEVICE_ADDRESS_BITS)

  lazy val image2DMaxWidth: Long = getDeviceLongParam(CL.CL_DEVICE_IMAGE2D_MAX_WIDTH)

  lazy val image2DMaxHeight: Long = getDeviceLongParam(CL.CL_DEVICE_IMAGE2D_MAX_HEIGHT)

  lazy val image3DMaxWidth: Long = getDeviceLongParam(CL.CL_DEVICE_IMAGE3D_MAX_WIDTH)

  lazy val image3DMaxHeight: Long = getDeviceLongParam(CL.CL_DEVICE_IMAGE3D_MAX_HEIGHT)

  lazy val image3DMaxDepth: Long = getDeviceLongParam(CL.CL_DEVICE_IMAGE3D_MAX_DEPTH)

  lazy val image3DMaxSamplers: Int = getDeviceIntParam(CL.CL_DEVICE_MAX_SAMPLERS)

  lazy val maxWorkItemDimensions: Int = getDeviceIntParam(CL.CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS)

  lazy val maxWorkItemSizes: Array[Long] = {
    val numDimensions = getDeviceIntParam(CL.CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS)
    val sizes = new Array[Long](numDimensions)
    val paramSize = sizes.length * 8
    val ret3 = Array(0L)
    CL.clGetDeviceInfo(id, CL.CL_DEVICE_MAX_WORK_ITEM_SIZES, paramSize, Pointer.to(sizes), ret3)
    sizes
  }

  lazy val workGroupSize: Long = getDeviceLongParam(CL.CL_DEVICE_MAX_WORK_GROUP_SIZE)

  private def getDeviceStringParam(param: Int) = {
    val bytes = new Array[Byte](1024)
    val paramSize = bytes.length
    val ret3 = Array(0L)
    CL.clGetDeviceInfo(id, param, paramSize, Pointer.to(bytes), ret3)
    new String(bytes, 0, (ret3(0) - 1).toInt, Charset.forName("UTF-8"))

  }

  private def getDeviceIntParam(param: Int) = {
    val ints = Array[Int](-1)
    val paramSize = ints.length * 4 // 4 bytes per int
    val paramValue = Pointer.to(ints)
    val ret3 = Array(0L)
    CL.clGetDeviceInfo(id, param, paramSize, paramValue, ret3)
    ints(0)
  }

  private def getDeviceLongParam(param: Int) = {
    val longs = Array[Long](-1)
    val paramSize = longs.length * 8 // 8 bytes per long
    val paramValue = Pointer.to(longs)
    val ret3 = Array(0L)
    CL.clGetDeviceInfo(id, param, paramSize, paramValue, ret3)
    longs(0)
  }
}

class DeviceType private[opencl](val descriptor: Long) {
  def isGpu: Boolean = (descriptor & CL.CL_DEVICE_TYPE_GPU) != 0

  def isCpu: Boolean = (descriptor & CL.CL_DEVICE_TYPE_CPU) != 0

  def isAccelerator: Boolean = (descriptor & CL.CL_DEVICE_TYPE_ACCELERATOR) != 0

  def isCustom: Boolean = (descriptor & CL.CL_DEVICE_TYPE_CUSTOM) != 0

  def isDefault: Boolean = (descriptor & CL.CL_DEVICE_TYPE_DEFAULT) != 0

  override def toString: String = {
    val buf = new StringBuilder
    if (isGpu) buf.append("GPU ")
    if (isCpu) buf.append("CPU ")
    if (isAccelerator) buf.append("Accelerator ")
    if (isCustom) buf.append("Custom ")
    if (isDefault) buf.append("Default ")
    if (buf.nonEmpty) {
      buf.slice(0, buf.size -1).toString() // trim trailing space
    } else {
      "Unknown"
    }
  }
}
