package sydney.hardy.graphics.opencl

import java.nio.charset.Charset

import org.jocl.CL._
import org.jocl.{CL, Pointer, cl_device_id, cl_platform_id}
import org.slf4j.LoggerFactory

/**
 * Platform represents the relationship to the operating system and OpenCL.
 * It contains high level information about OpenCL support a machine.
 *
 * @see [[sydney.hardy.graphics.opencl.Platform.all]] to obtain a list of platforms present.
 *
 * @param platformId - The ID of the platform as supplied by OpenCL.
 */
case class Platform(platformId: cl_platform_id) {

  lazy val name: String = getPlatformInfoString(CL_PLATFORM_NAME)

  lazy val profile: String = getPlatformInfoString(CL_PLATFORM_PROFILE)

  lazy val vendor: String = getPlatformInfoString(CL_PLATFORM_VENDOR)

  lazy val extensions: String = getPlatformInfoString(CL_PLATFORM_EXTENSIONS)

  lazy val version: String = getPlatformInfoString(CL_PLATFORM_VERSION)

  /**
   * A list of the IDs of all the GPUs present in the platform
   */
  lazy val deviceIds: List[cl_device_id] = {
    val maxDevices = 20
    val ids = new Array[cl_device_id](maxDevices)
    val numDevices = Array[Int](1)
    CL.clGetDeviceIDs(platformId, CL.CL_DEVICE_TYPE_GPU, ids.length, ids, numDevices)
    ids.slice(0, numDevices(0)).toList
  }

  lazy val devices: List[Device] = {
    deviceIds.map(id => new Device(id, this))
  }

  private def getPlatformInfoString(key: Int): String = {
    val resultBuffer = new Array[Byte](1024)
    val resultLength = new Array[Long](1)
    CL.clGetPlatformInfo(platformId, key, resultBuffer.length.toLong, Pointer.to(resultBuffer), resultLength)
    new String(resultBuffer, 0, resultLength(0).toInt - 1, Charset.forName("UTF-8"))
  }
}

object Platform {
  val LOG = LoggerFactory.getLogger(classOf[Platform])
  val MAX_PLATFORMS = 10

  lazy val all: List[Platform] = {
    val platformIds = new Array[cl_platform_id](MAX_PLATFORMS)
    val platformCountReturn = Array[Int](1)
    val err = CL.clGetPlatformIDs(MAX_PLATFORMS, platformIds, platformCountReturn)
    if (err != CL_SUCCESS) {
      LOG.error("Couldn't get platform list: {}, {}", err, CL.stringFor_cl_platform_info(err))
      throw new IllegalStateException("Couldn't get platform list.")
    }

    val platformCount = platformCountReturn(0)
    if (platformCount == 0) {
      throw new IllegalStateException("No OpenCL Platform found.")
    }
    platformIds.slice(0, platformCount).toList.map(Platform(_))
  }
}
