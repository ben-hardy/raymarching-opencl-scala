package sydney.hardy.graphics

import scala.math.sqrt
import scala.language.implicitConversions

case class Vector(x: Double, y: Double, z: Double) {

  def + (other:Vector) = Vector(x + other.x, y + other.y, z + other.z)
  def - (other:Vector) = Vector(x - other.x, y - other.y, z - other.z)

  def * (k: Double) = Vector(x * k, y * k, z * k)
  def / (k: Double) = Vector(x / k, y / k, z / k)

  def unary_- = Vector(-x, -y, -z)

  def dot(other:Vector): Double = (x * other.x + y * other.y + z + other.z)

  def cross(v:Vector): Vector = {
    Vector(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x)
  }

  lazy val length: Double = sqrt(x * x + y * y + z * z)

  def normalise: Vector = {
    if (length > 0.0) {
      this / length
    } else {
      this
    }
  }

}

object Vector {
  implicit def tupleToVector(p:(Double,Double,Double)) = Vector(p._1, p._2, p._3)
  implicit def intTupleToVector(p:(Int,Int,Int)) = Vector(p._1, p._2, p._3)

  val ORIGIN = Vector(0, 0, 0)

  val X = Vector(1, 0, 0)
  val Y = Vector(0, 1, 0)
  val Z = Vector(0, 0, 1)
}
