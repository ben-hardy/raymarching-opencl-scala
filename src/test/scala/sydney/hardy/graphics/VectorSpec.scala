package sydney.hardy.graphics

import org.scalatest.funspec.AnyFunSpec
import sydney.hardy.graphics.Vector.{ORIGIN, X, Y, Z}

import scala.math.{cos, sin, toRadians}

class VectorSpec extends AnyFunSpec {
  describe("a Vector") {
    describe("addition") {
      it("adds each component") {
        assert(Vector(1, 2, 3) + Vector(10, 20, 30) == Vector(11, 22, 33))
      }
      it("is commutative") {
        assert(Vector(1, 2, 3) + Vector(10, 20, 30) == (Vector(10, 20, 30) + Vector(1, 2, 3)))
      }
    }
    describe("subtraction") {
      it("adds each component") {
        assert(Vector(1, 2, 3) + Vector(10, 20, 30) == Vector(11, 22, 33))
      }
      it("is not commutative") {
        assert(Vector(1, 2, 3) - Vector(10, 20, 30) != Vector(10, 20, 30) - Vector(1, 2, 3))
      }
    }
    describe("scalar multiplication") {
      it("multiplies each component") {
        assert(Vector(1, 2, 3) * 10 == Vector(10, 20, 30))
      }
    }
    describe("scalar division") {
      it("divides each component") {
        assert(Vector(10, 20, 30) / 10 == Vector(1, 2, 3))
      }
      it("division by zero results in infinity") {
        val v = Vector(1, 2, 3) / 0.0
        assert(v.x.isInfinite)
        assert(v.y.isInfinite)
        assert(v.z.isInfinite)
      }
    }
    describe("unary -") {
      it("flips sign of components") {
        assert(-Vector(1, 2, 3) == Vector(-1, -2, -3))
      }
    }
    describe("length") {
      it("completes Pythagorean 4-tuple") {
        assert(Vector(4, 13, 16).length == 21.0)
        assert(Vector(-4, -13, -16).length == 21.0)
      }
      it("is 1 for a unit vector along X") {
        assert(X.length == 1.0)
      }
    }
    describe("normalised") {
      it("produces a unit vector with same proportions as source") {
        val v = Vector(1, 4, 8)
        assert(v.length == 9.0)
        val vn = v.normalise
        assert(vn.length == 1.0)
        assert(vn * 9.0 == v)
      }
      it("is 1 for a unit vector along X") {
        assert(X.length == 1.0)
      }
    }
    describe("dot product") {
      it("produces 0 for perpendicular vectors") {
        val v = Vector(-5, 5, 0)
        val w = Vector(5, 5, 0)
        assert((v dot w) == 0.0)
      }
      it("is 1 for co-linear unit vectors") {
        val angle = toRadians(30)
        val v = Vector(cos(angle), sin(angle), 0.0)
        assert((v dot v) == 1.0)
      }
      it("is radius squared for co-linear vectors") {
        val angle = toRadians(30)
        val radius = 2.0
        val v = Vector(cos(angle), sin(angle), 0.0) * radius
        assert((v dot v) == radius * radius)
      }
      it("can be used to calculate projection") {
        val angle = toRadians(30)
        val radius = 10.0
        val v = Vector(cos(angle), sin(angle), 0.0) * radius
        // projection of v along X
        val p = X * (X dot v)
        assert(p == Vector(cos(angle) * radius, 0.0, 0.0))
      }
    }
    describe("cross product") {
      it("X cross Y = Z") {
        assert((X cross Y) == Z)
      }
      it("Y cross Z = X") {
        assert((X cross Y) == Z)
      }
      it("Z cross X = Y") {
        assert((X cross Y) == Z)
      }
      it("Y cross X = -Z") {
        assert((Y cross X) == -Z)
      }
      it("Z cross Y = -X") {
        assert((Z cross Y) == -X)
      }
      it("X cross Z = -Y") {
        assert((X cross Z) == -Y)
      }
    }
  }
  describe("Origin") {
    it("should have length 0") {
      assert(ORIGIN.length == 0.0)
    }
    describe("addition") {
      it("yields itself when added to itself") {
        assert(ORIGIN + ORIGIN == ORIGIN)
      }
      it("yields other when added to other") {
        val other = Vector(1, 2, 3)
        assert(other + ORIGIN == other)
      }
      it("yields other when other added to it") {
        val other = Vector(1, 2, 3)
        assert(other + ORIGIN == other)
      }
    }
    describe("subtraction") {
      it("yields itself when subtracted from itself") {
        assert(ORIGIN - ORIGIN == ORIGIN)
      }
      it("yields -other when other subtracted from it") {
        val other = Vector(1, 2, 3)
        assert(ORIGIN - other == -other)
      }
      it("yields other when subtracted from other") {
        val other = Vector(1, 2, 3)
        assert(other - ORIGIN == other)
      }
    }
  }
  describe("Companion object") {
    describe("tupleToVector") {
      it("converts tuples of doubles to Vectors") {
        val t = (1.0, 2.0, 3.0)
        val v = Vector(1.0, 2.0, 3.0)
        assert(v == Vector(t._1, t._2, t._3))
      }
      it("does implicit conversions for Double tuples") {
        val v = Vector(10, 20, 30)
        val v2 = v + (1.0, 2.0, 3.0)
        assert(v2 == Vector(11, 22, 33))
      }
    }
    describe("intTupleToVector") {
      it ("does implicit conversions for Int tuples") {
        val v = Vector(10, 20, 30)
        val v2 = v + (1, 2, 3)
        assert(v2 == Vector(11, 22, 33))
      }
    }
  }
}
